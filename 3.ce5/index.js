const vm = new Vue({
    el: '#root',
    data(){
        return{
            isOpen: false
        }
    },
    methods:{
        openButtonClicked(){
            this.isOpen = true;
        },
        dropClicked(){
            this.isOpen = false;
        }
    },
    template: `
        <div>
        <a class='dropdown-button btn' v-if="!isOpen" href='#' @click="openButtonClicked">Open</a>
        
        <ul class='dropdown-content' v-if="isOpen">
            <li><a href="#!" @click="dropClicked">one</a></li>
            <li><a href="#!" @click="dropClicked">two</a></li>
            <li class="divider" @click="dropClicked"></li>
            <li><a href="#!" @click="dropClicked">three</a></li>
        </ul>
        </div>
    `
});
