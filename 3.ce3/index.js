// Add a method called 'logPI' to the Vue instance's 'methods' object.
// Call this method any time the button is clicked
const vm = new Vue({
    el: '#root',
    methods:{
        logPI(){
            console.log(Math.PI);
        }
    },
    template: 
    `
        <div>
            Click the button to log out the value of PI
            <button @click="logPI">Click Me</button>
        </div>
    `
});