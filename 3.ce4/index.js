const vm = new Vue({
    el: '#root',
    data(){
        return{
            count: 0
        }
    },
    methods: {
        increment(){
            this.count++;
        },
        decrement(){
            this.count--;
        }
    },
    template: `
        <div>
            Current count: {{ this.count }}
            <div>
                <button @click="decrement">-</button>
                <button @click="increment">+</button>
            </div>
        </div>
    `
});