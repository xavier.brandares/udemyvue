// Make the template show a ':(' if it isn't Friday
// If it is Friday, then make it show a ':)'
const vm = new Vue({
    el: '#root',
    computed: {
        isFriday: function(){
            const date = new Date().getDay();
            if(date === 5){
                return ":)";
            }
            else{
                return ":(";
            }
        }
        
    },
    template: `
        <div class="date">
            <div v-html='isFriday'></div>
        </div>
    `
});